#!/bin/ksh

########################################################################
# Name          : Amss2Mec.ksh 
#
# Description   : Executing the notification message extract process 
#		  which will create list of XMLs per each retrieved BAN.
# 
# Author        : Suman Mandal
#
# Date          : 7/9/2020
######################################################################## 

#Check if Amss2Mec process already run.

if [ "`ps -fu ${USER} | grep com.amdocs.css.impl.R65.batch.ebillNotification.ImplAmss2MecProcess | wc -l`" -gt 1 ];then
   echo ""
   echo "Should be run only one process of ImplAmss2MecProcess on the same host."
   echo "Please stop the old process before running a new one."
   echo ""
   exit 114
fi 

. ${AMSS_BATCH_PROPERTIES_HOME}/impl9Setenv.sh


export PROCESS_NAME=com.amdocs.css.impl.R65.batch.ebillNotification.ImplAmss2MecProcess

# Checking input
help(){
   echo "Usage: ${PROCESS_NAME} [file_path] [FTG_Dir_path]"
   echo "file_path - represents path to directory where threads should create XML and CSV files"
   echo "FTG_Dir_path - represents path to the directory where final tar.gz file should be placed to be handled by FTG"
   exit 22
}

if [[ $# -eq 0 ]];then
	echo "No Arguments"
	help
fi

if [[ $1 = "-h" || $1 = "-help" ]];then
	help
fi 

if [[ -d $1 && -w $1 && -d $2 && -w $2 ]];then
	echo "Correct input arguments"
else
	echo "Wrong Arguments: $1 and $2 are not directories or does not have write permissions."
	help
fi 

if [[ -f ${1}/StopAmss2Mec.txt ]];then
	echo "Removing file ${1}/StopAmss2Mec.txt"
	rm -f ${1}/StopAmss2Mec.txt
fi

echo "Running Process: ${PROCESS_NAME}"

export JAVA_ARGS="-Xms256m -Xmx512m"
export JAVA_ARGS="${JAVA_ARGS} -Dserver.type=BATCH"
#export JAVA_ARGS="${JAVA_ARGS} -classpath ${BATCH_CLASSPATH}"
export JAVA_ARGS="${JAVA_ARGS} -Damdocs.system.home=${AMSS_BATCH_PROPERTIES_HOME}"
export JAVA_ARGS="${JAVA_ARGS} -Damdocs.csc.processname=$PROCESS_NAME"
export JAVA_ARGS="${JAVA_ARGS} -classpath ${BATCH_CLASSPATH}"
export JAVA_ARGS="${JAVA_ARGS} -Damdocs.messageHandling.home=${AMSS_BATCH_PROPERTIES_HOME}"
export JAVA_ARGS="${JAVA_ARGS} ${JVM_ARGUMENTS}"
export JAVA_ARGS="${JAVA_ARGS} ${JVM_BATCH_ARGUMENTS}"


#DEBUG_ARGS="-Xdebug -Xnoagent -Djava.compiler=NONE -Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=15918"

#JAVA_ARGS="${JAVA_ARGS} ${DEBUG_ARGS}"
#JAVA_ARGS="${JAVA_ARGS} -DDEBUG=Y"
#JAVA_ARGS="${JAVA_ARGS} -DDEBUG_MSG=ON"

# Generating log file name
LOG_FILE_NAME="${AMSS_BATCH_LOG_HOME}/${PROCESS_NAME}_`date +%Y%m%d%H%M%S`.log"

COMMAND="/usr/java/latest/bin/java"
COMMAND="${COMMAND} ${JAVA_ARGS}"
COMMAND="${COMMAND} com.amdocs.css.impl.R65.batch.ebillNotification.ImplAmss2MecProcess $1 $DB_HOST $DB_PORT $DB_INST $LOG_FILE_NAME"


#echo "Classpath:" 
#echo ${BATCH_CLASSPATH}

#echo "Running Comand:" 
#echo ${COMMAND}

while true

do

${COMMAND} 
echo "Command completed, now working with Tars" 
# If JAVA process was terminated manually - exit without processing XML and CSV files
if [[ -f ${1}/StopAmss2Mec.txt ]];then
	echo "Removing file ${1}/StopAmss2Mec.txt and exiting" >> $LOG_FILE_NAME
	echo "Exiting due to manual stop"  >> $LOG_FILE_NAME
	rm -f ${1}/StopAmss2Mec.txt
	exit 0
fi

# Check if there are XML files created, if not then do not create TAR and remove CSV
if [ "`find "$1" -type f -name "*.xml" | wc -l`" -gt 0 ];then
	#Creating a tar.gz file of XMLs and CSV altogether
	csvSysDate=`date "+%Y%m%d_%H%M%S"`
	tarGzFile="NM1ebillReadyNotification_${csvSysDate}.tar.gz"
	tar -pczf "$2"/$tarGzFile -C "$1" .
	
	#In case of an error - remove corrupted tar.gz files
	if [[ $? != 0 ]]; then
		echo "Cannot create tar.gz file $tarGzFile">> $LOG_FILE_NAME
		rm -f $tarGzFile
		exit 1	
	fi
fi

#Now we can remove all XML and CSV files
#rm -f ${1}/*.xml ${1}/*.csv
rm -r ${1}
mkdir ${1}

#In case of abortion, remove stopping file
if [[ -f ${1}/StopAmss2Mec.txt ]];then
	echo "In Abortion if statement" >> $LOG_FILE_NAME
	echo "Removing file ${1}/StopAmss2Mec.txt" >> $LOG_FILE_NAME
	rm -f ${1}/StopAmss2Mec.txt
	exit 0
fi
echo "Going to Sleep"  >> $LOG_FILE_NAME
sleep 3600
done
